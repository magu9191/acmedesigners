<?php

/* themes/acmedesigners/templates/page.html.twig */
class __TwigTemplate_0ac1843ba04e6b1eb04650c035dd0c60782cc54971991d6f8b84d76e97b21521 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'main' => array($this, 'block_main'),
            'header' => array($this, 'block_header'),
            'sidebar_first' => array($this, 'block_sidebar_first'),
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'action_links' => array($this, 'block_action_links'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'sidebar_second' => array($this, 'block_sidebar_second'),
            'teritary' => array($this, 'block_teritary'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 59, "if" => 63, "block" => 64);
        $filters = array("clean_class" => 69, "t" => 81);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 59
        $context["container"] = (($this->getAttribute($this->getAttribute((isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 61
        echo "
<div class=\"custom-top-header\">
";
        // line 63
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array()))) {
            // line 64
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 101
        echo "</div>

";
        // line 103
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "topheader", array())) {
            // line 104
            echo "\t<div class=\"custom-header\">
\t\t";
            // line 105
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "topheader", array()), "html", null, true));
            echo "
\t</div>
";
        }
        // line 108
        echo "\t  

";
        // line 111
        $this->displayBlock('main', $context, $blocks);
        // line 190
        echo "
";
        // line 191
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "teritary", array())) {
            // line 192
            echo "  ";
            $this->displayBlock('teritary', $context, $blocks);
        }
        // line 196
        echo "
";
        // line 197
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array())) {
            // line 198
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
    }

    // line 64
    public function block_navbar($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        // line 66
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 68
(isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 69
(isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_position", array())) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute($this->getAttribute((isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_position", array())))) : ((isset($context["container"]) ? $context["container"] : null))));
        // line 72
        echo "    <header";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["navbar_attributes"]) ? $context["navbar_attributes"] : null), "addClass", array(0 => (isset($context["navbar_classes"]) ? $context["navbar_classes"] : null)), "method"), "html", null, true));
        echo " id=\"navbar\" role=\"banner\">
      ";
        // line 73
        if ( !$this->getAttribute((isset($context["navbar_attributes"]) ? $context["navbar_attributes"] : null), "hasClass", array(0 => (isset($context["container"]) ? $context["container"] : null)), "method")) {
            // line 74
            echo "        <div class=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["container"]) ? $context["container"] : null), "html", null, true));
            echo "\">
      ";
        }
        // line 76
        echo "      <div class=\"navbar-header\">
        ";
        // line 77
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()), "html", null, true));
        echo "
        ";
        // line 79
        echo "        ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array())) {
            // line 80
            echo "          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
            <span class=\"sr-only\">";
            // line 81
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation")));
            echo "</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
        ";
        }
        // line 87
        echo "      </div>

      ";
        // line 90
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array())) {
            // line 91
            echo "        <div id=\"navbar-collapse\" class=\"navbar-collapse collapse\">
          ";
            // line 92
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array()), "html", null, true));
            echo "
        </div>
      ";
        }
        // line 95
        echo "      ";
        if ( !$this->getAttribute((isset($context["navbar_attributes"]) ? $context["navbar_attributes"] : null), "hasClass", array(0 => (isset($context["container"]) ? $context["container"] : null)), "method")) {
            // line 96
            echo "        </div>
      ";
        }
        // line 98
        echo "    </header>
  ";
    }

    // line 111
    public function block_main($context, array $blocks = array())
    {
        // line 112
        echo "  <div role=\"main\" class=\"main-container ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["container"]) ? $context["container"] : null), "html", null, true));
        echo " js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 116
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array())) {
            // line 117
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 122
            echo "      ";
        }
        // line 123
        echo "
      ";
        // line 125
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())) {
            // line 126
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 131
            echo "      ";
        }
        // line 132
        echo "
      ";
        // line 134
        echo "      ";
        // line 135
        $context["content_classes"] = array(0 => ((($this->getAttribute(        // line 136
(isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()) && $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array()))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 137
(isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()) && twig_test_empty($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array())))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 138
(isset($context["page"]) ? $context["page"] : null), "sidebar_second", array()) && twig_test_empty($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 139
(isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())) && twig_test_empty($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array())))) ? ("col-sm-12") : ("")));
        // line 142
        echo "      <section";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["content_attributes"]) ? $context["content_attributes"] : null), "addClass", array(0 => (isset($context["content_classes"]) ? $context["content_classes"] : null)), "method"), "html", null, true));
        echo ">

        ";
        // line 145
        echo "        ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array())) {
            // line 146
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 149
            echo "        ";
        }
        // line 150
        echo "
        ";
        // line 152
        echo "        ";
        if ((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null)) {
            // line 153
            echo "          ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 156
            echo "        ";
        }
        // line 157
        echo "
        ";
        // line 159
        echo "        ";
        if ((isset($context["action_links"]) ? $context["action_links"] : null)) {
            // line 160
            echo "          ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 163
            echo "        ";
        }
        // line 164
        echo "
        ";
        // line 166
        echo "        ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array())) {
            // line 167
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 170
            echo "        ";
        }
        // line 171
        echo "
        ";
        // line 173
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 177
        echo "      </section>

      ";
        // line 180
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array())) {
            // line 181
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 186
            echo "      ";
        }
        // line 187
        echo "    </div>
  </div>
";
    }

    // line 117
    public function block_header($context, array $blocks = array())
    {
        // line 118
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 119
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
          </div>
        ";
    }

    // line 126
    public function block_sidebar_first($context, array $blocks = array())
    {
        // line 127
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 128
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()), "html", null, true));
        echo "
          </aside>
        ";
    }

    // line 146
    public function block_highlighted($context, array $blocks = array())
    {
        // line 147
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
        echo "</div>
          ";
    }

    // line 153
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 154
        echo "            ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "html", null, true));
        echo "
          ";
    }

    // line 160
    public function block_action_links($context, array $blocks = array())
    {
        // line 161
        echo "            <ul class=\"action-links\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["action_links"]) ? $context["action_links"] : null), "html", null, true));
        echo "</ul>
          ";
    }

    // line 167
    public function block_help($context, array $blocks = array())
    {
        // line 168
        echo "            ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array()), "html", null, true));
        echo "
          ";
    }

    // line 173
    public function block_content($context, array $blocks = array())
    {
        // line 174
        echo "          <a id=\"main-content\"></a>
          ";
        // line 175
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
        ";
    }

    // line 181
    public function block_sidebar_second($context, array $blocks = array())
    {
        // line 182
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 183
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array()), "html", null, true));
        echo "
          </aside>
        ";
    }

    // line 192
    public function block_teritary($context, array $blocks = array())
    {
        // line 193
        echo "      <div class=\"teritary-content\">";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "teritary", array()), "html", null, true));
        echo "</div>
  ";
    }

    // line 198
    public function block_footer($context, array $blocks = array())
    {
        // line 199
        echo "    <footer class=\"footer ";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["container"]) ? $context["container"] : null), "html", null, true));
        echo "\" role=\"contentinfo\">
      <div class=\"Custom-footer container\">";
        // line 200
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
        echo "</div>
    </footer>
  ";
    }

    public function getTemplateName()
    {
        return "themes/acmedesigners/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  407 => 200,  402 => 199,  399 => 198,  392 => 193,  389 => 192,  382 => 183,  379 => 182,  376 => 181,  370 => 175,  367 => 174,  364 => 173,  357 => 168,  354 => 167,  347 => 161,  344 => 160,  337 => 154,  334 => 153,  327 => 147,  324 => 146,  317 => 128,  314 => 127,  311 => 126,  304 => 119,  301 => 118,  298 => 117,  292 => 187,  289 => 186,  286 => 181,  283 => 180,  279 => 177,  276 => 173,  273 => 171,  270 => 170,  267 => 167,  264 => 166,  261 => 164,  258 => 163,  255 => 160,  252 => 159,  249 => 157,  246 => 156,  243 => 153,  240 => 152,  237 => 150,  234 => 149,  231 => 146,  228 => 145,  222 => 142,  220 => 139,  219 => 138,  218 => 137,  217 => 136,  216 => 135,  214 => 134,  211 => 132,  208 => 131,  205 => 126,  202 => 125,  199 => 123,  196 => 122,  193 => 117,  190 => 116,  183 => 112,  180 => 111,  175 => 98,  171 => 96,  168 => 95,  162 => 92,  159 => 91,  156 => 90,  152 => 87,  143 => 81,  140 => 80,  137 => 79,  133 => 77,  130 => 76,  124 => 74,  122 => 73,  117 => 72,  115 => 69,  114 => 68,  113 => 66,  111 => 65,  108 => 64,  102 => 198,  100 => 197,  97 => 196,  93 => 192,  91 => 191,  88 => 190,  86 => 111,  82 => 108,  76 => 105,  73 => 104,  71 => 103,  67 => 101,  63 => 64,  61 => 63,  57 => 61,  55 => 59,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "themes/acmedesigners/templates/page.html.twig", "C:\\wamp\\www\\acmedesigners\\themes\\acmedesigners\\templates\\page.html.twig");
    }
}
