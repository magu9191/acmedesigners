<?php

/* {# inline_template_start #}<div class="col-sm-7 col-xs-12">
<div class="HA-title">{{ title }} </div>
<div class="HA-content">{{ body }}</div>
</div>
<div class="col-sm-5 col-xs-12">
<p>AWARD WINNING ARCHITECTURAL FIRM</p>
{{ field_page_image }}
</div> */
class __TwigTemplate_b7cdea8afba64d3d45932dabdfba6eb0fd2d3951c6ef980bd13fd1a360b5e833 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"col-sm-7 col-xs-12\">
<div class=\"HA-title\">";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
        echo " </div>
<div class=\"HA-content\">";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["body"]) ? $context["body"] : null), "html", null, true));
        echo "</div>
</div>
<div class=\"col-sm-5 col-xs-12\">
<p>AWARD WINNING ARCHITECTURAL FIRM</p>
";
        // line 7
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["field_page_image"]) ? $context["field_page_image"] : null), "html", null, true));
        echo "
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"col-sm-7 col-xs-12\">
<div class=\"HA-title\">{{ title }} </div>
<div class=\"HA-content\">{{ body }}</div>
</div>
<div class=\"col-sm-5 col-xs-12\">
<p>AWARD WINNING ARCHITECTURAL FIRM</p>
{{ field_page_image }}
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 7,  57 => 3,  53 => 2,  50 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "{# inline_template_start #}<div class=\"col-sm-7 col-xs-12\">
<div class=\"HA-title\">{{ title }} </div>
<div class=\"HA-content\">{{ body }}</div>
</div>
<div class=\"col-sm-5 col-xs-12\">
<p>AWARD WINNING ARCHITECTURAL FIRM</p>
{{ field_page_image }}
</div>", "");
    }
}
